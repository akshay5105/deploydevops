package com.dpt.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import java.util.Collections;

public class TestMain {
    public WebDriver driver;
    @BeforeTest
    public void test(){
        WebDriverManager.chromedriver().setup();

        ChromeOptions options = new ChromeOptions();
        options.setBinary("/usr/bin/google-chrome");  //chrome binary location specified here
       options.addArguments("--headless");
        options.addArguments("window-size=1400,1500");
        options.addArguments("--disable-gpu");
        options.addArguments("--no-sandbox");
        options.addArguments("start-maximized");
        options.addArguments("enable-automation");
        options.addArguments("--disable-infobars");
        options.addArguments("--disable-dev-shm-usage");
        driver = new ChromeDriver(options);

    }

    @Test
    public void test1(){
        driver.get("http://54.82.234.201:8080/dptweb-2.0/");
        try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String actual = driver.findElement(By.xpath("//a[contains(.,'Login Here')]")).getText();
        Assert.assertEquals(actual,"Login Here");
        System.out.println("----------------------------------------------------------");
       System.out.println("-----------PASSS------------");
       System.out.println("----------------------------------------------------------");
driver.close();

        driver.quit();





    }



}
